def sqrt(x: Double, epsilon: Double = 1e-4, guess: Double = 1.0): Double = {
  val isGoodEnough = Math.abs(guess * guess - x) / x < epsilon
  def improvedGuess(): Double = (guess + x/guess) / 2

  if (isGoodEnough) guess
  else sqrt(x, epsilon, improvedGuess())
}

// Some test cases
var tests = List(1e-3, .1e-20, 1e20, 1e50)
tests.map(x => sqrt(x) - Math.sqrt(x))